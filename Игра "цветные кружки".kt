package com.example.circle

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(GameView(this))
    }
}

data class Circle(var x: Float, var y: Float, val radius: Float, val color: Int, val originalX: Float, val originalY: Float) {
    fun contains(px: Float, py: Float): Boolean {
        val dx = px - x
        val dy = py - y
        return dx * dx + dy * dy <= radius * radius
    }
}

class GameView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    private val circles = mutableListOf<Circle>()
    private val random = Random.Default
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var draggingCircle: Circle? = null
    private var offsetX = 0f
    private var offsetY = 0f

    private var targetColor: Int = Color.RED
    private val targetRect = RectF()

    private val availableColors = listOf(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN)

    init {
        updateTargetColor()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (circles.isEmpty()) {
            generateCircles()
            updateTargetColor()
        }

        targetRect.set(width - 300f, height / 2f - 100f, width - 100f, height / 2f + 100f)
    }

    private fun generateCircles() {
        circles.clear()
        val colors = availableColors.shuffled().take(5) // Уникальные цвета
        repeat(5) {
            val safeX = random.nextInt(100, width - 100).toFloat()
            val safeY = random.nextInt(100, height - 100).toFloat()
            circles.add(Circle(safeX, safeY, 50f, colors[it], safeX, safeY))
        }
    }

    private fun updateTargetColor() {
        targetColor = if (circles.isNotEmpty()) circles.random().color else Color.TRANSPARENT
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        for (circle in circles) {
            paint.color = circle.color
            canvas.drawCircle(circle.x, circle.y, circle.radius, paint)
        }

        paint.color = targetColor
        canvas.drawRect(targetRect, paint)

        if (circles.isEmpty()) {
            paint.color = Color.WHITE
            paint.textSize = 64f
            canvas.drawText("Игра окончена!", width / 2f - 200f, height / 2f, paint)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                draggingCircle = circles.find { it.contains(event.x, event.y) }
                draggingCircle?.let {
                    offsetX = it.x - event.x
                    offsetY = it.y - event.y
                }
            }
            MotionEvent.ACTION_MOVE -> {
                draggingCircle?.let {
                    it.x = event.x + offsetX
                    it.y = event.y + offsetY
                    invalidate()
                }
            }
            MotionEvent.ACTION_UP -> {
                draggingCircle?.let {
                    if (it.color == targetColor && targetRect.contains(it.x, it.y)) {
                        circles.remove(it)
                        updateTargetColor()
                    } else {
                        it.x = it.originalX
                        it.y = it.originalY
                    }
                    draggingCircle = null
                    invalidate()
                }
            }
        }
        return true
    }
}
