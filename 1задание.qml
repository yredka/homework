import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 400
    height: 600
    title: "Task for Login Page"

    Column {
        anchors.centerIn: parent
        spacing: 10

        TextField {
            id: usernameField
            placeholderText: "Username"
            width: 200
        }

        TextField {
            id: passwordField
            placeholderText: "Password"
            echoMode: TextInput.Password
            width: 200
        }

        Row {
            spacing: 10

            Button {
                text: "Log In"
                onClicked: {
                   
                    console.log("Log In button clicked")
                }
            }

            Button {
                text: "Clear"
                onClicked: {
                  
                    usernameField.text = ""
                    passwordField.text = ""
                }
            }
        }
    }
}
