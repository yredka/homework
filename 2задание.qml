import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 300
    height: 500
    title: "Task for Login Page"

    property string password: ""

    Column {
        anchors.centerIn: parent
        spacing: 10

        Text {
            text: "Enter your password:"
            font.pointSize: 16
        }

        Row {
            id: passwordRow
            spacing: 5
            Repeater {
                model: 6
                Label {
                    id: passwordChar
                    text: "•"
                    color: password.length >= modelData ? "black" : "lightgray"
                    font.pixelSize: 20
                }
            }
        }

        Grid {
            columns: 3
            spacing: 5
            Repeater {
                model: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Clear"]
                Button {
                    text: modelData
                    width: 60
                    height: 60
                    onClicked: {
                        if (text === "Clear") {
                            password = ""
                        } else {
                            if (password.length < 6) {
                                password += text
                            }
                        }
                    }
                }
            }
        }

        Button {
            text: "Log In"
            width: 100
            height: 40
            onClicked: {

                console.log("Log In button clicked, password:", password)
            }
        }
    }
}
