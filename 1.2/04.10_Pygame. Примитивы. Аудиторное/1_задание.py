import pygame

pygame.init()

scr = pygame.display.set_mode((300, 300))

#клавиши
keys = {
    pygame.K_RETURN: 'enter',
    pygame.K_SPACE: 'space',
    pygame.K_w: 'w',
    pygame.K_a: 'a',
    pygame.K_s: 's',
    pygame.K_d: 'd',
    pygame.K_ESCAPE: 'esc',
    pygame.K_LEFT: 'left',
    pygame.K_RIGHT: 'right',
    pygame.K_UP: 'up',
    pygame.K_DOWN: 'down'
}

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

        #клавиша нажата
        if event.type == pygame.KEYDOWN:
            if event.key in keys:
                print(keys[event.key])

