import pygame
from pygame.locals import *
pygame.init()

window = pygame.display.set_mode((600, 600))
  
clock = pygame.time.Clock()
  
direction = True

#картинки для движения вправо и влево  
image_left = [pygame.image.load('animations/l1.png'),
            pygame.image.load('animations/l2.png'),
           pygame.image.load('animations/l3.png'),
           pygame.image.load('animations/l4.png'),
           pygame.image.load('animations/l5.png'),]
image_right = [pygame.image.load('animations/r1.png'),
               pygame.image.load('animations/r1.png'),
           pygame.image.load('animations/r2.png'),
           pygame.image.load('animations/r3.png'),
           pygame.image.load('animations/r4.png'),
           pygame.image.load('animations/r5.png')]

x = 100
y = 100
current_img=0
  
run = True
while run:
    clock.tick(20)

    window.fill((255, 255, 255))

    #смена кадров соответствующих направлению
    if direction == True:
        current_img = (current_img + 1) % len(image_right)
        window.blit(image_right[current_img], (x, y))
    if direction == False:
        current_img = (current_img + 1) % len(image_left)
        window.blit(image_left[current_img], (x, y))
  
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()
            quit()
            
        #определение направления
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                direction = True
            elif event.key == pygame.K_LEFT:
                direction = False
                
    #движение перса
    key_pressed = pygame.key.get_pressed()

    if key_pressed[K_LEFT]:
        x -= 5
    if key_pressed[K_RIGHT]:
        x += 5
    if key_pressed[K_UP]:
        y -= 5
    if key_pressed[K_DOWN]:
        y += 5

    pygame.display.update()
