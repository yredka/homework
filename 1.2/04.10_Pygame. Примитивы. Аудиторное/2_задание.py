import pygame

pygame.init()

# цвета
red = 'darkred'
brown = 'saddlebrown'
beige = 'bisque'
green = 'darkolivegreen'
blue = 'skyblue'
purple = 'mediumpurple'
black = 'black'

scr = pygame.display.set_mode((300, 300))

#начальные координаты
x = 145
y = 145
speed = 5
prev_x = x
prev_y = y

color = red
trail_points = []

game_stop = False
clock = pygame.time.Clock()

while not game_stop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_stop = True

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                x -= speed
            elif event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                x += speed
            elif event.key == pygame.K_UP or event.key == pygame.K_w:
                y -= speed
            elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
                y += speed
            elif event.key == pygame.K_c:
                # Смена цвета квадрата
                if color == red:
                    color = green
                elif color == green:
                    color = blue
                elif color == blue:
                    color = beige
                elif color == beige:
                    color = purple
                elif color == purple:
                    color =black
                else:
                    color = red

    # Проверка на выход за пределы экрана
    if x < 0:
        x = 0
    elif x > 290:
        x = 290
    elif y < 0:
        y = 0
    elif y > 290:
        y = 290

    # Рисование квадрата и его следа

    scr.fill('black')
    pygame.draw.rect(scr, color, [x, y, 10, 10])
    pygame.draw.rect(scr, color, [prev_x, prev_y, 10, 10])
    trail_points.append((x + 4, y + 4))
    for i in range(1, len(trail_points)):
        pygame.draw.line(scr, color, trail_points[i-1], trail_points[i], 10)
    
    pygame.display.flip()

    clock.tick(60)

pygame.quit()
