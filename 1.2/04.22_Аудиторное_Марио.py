import pygame

pygame.init()

top_surf=pygame.display.set_mode((600,60))
game_clock=pygame.time.Clock()
top_surf.fill((255, 255, 255))

m1=pygame.sprite.Sprite()
m1.image=pygame.image.load('./animations/0.png')
m1.rect=m1.image.get_rect()
m1.rect.move_ip(20, 0)

m2=pygame.sprite.Sprite()
m2.image=pygame.image.load('./animations/0.png')
m2.rect=m2.image.get_rect()
m2.rect.move_ip(570, 0)

image_right = ["./animations/r1.png",
               "./animations/r2.png",
               "./animations/r3.png",
               "./animations/r4.png",
               "./animations/r5.png"]
image_left = ["./animations/l1.png",
              "./animations/l2.png",
              "./animations/l3.png",
              "./animations/l4.png",
              "./animations/l5.png"]

runing=True
while runing:
    top_surf.fill((255, 255, 255))
    top_surf.blit(m1.image, m1.rect)
    top_surf.blit(m2.image, m2.rect)
    if pygame.sprite.collide_rect(m1, m2):
        quit()
        
    if pygame.key.get_pressed()[pygame.K_d]:
        for i in image_right:
            m1.image=pygame.image.load(i)
            image_right.remove(i)
            image_right.append(i)
            m1.rect.move_ip(1, 0)
    if pygame.key.get_pressed()[pygame.K_a]:
        for i in image_left:
            m1.image=pygame.image.load(i)
            image_left.remove(i)
            image_left.append(i)
            m1.rect.move_ip(-1, 0)
    if True not in pygame.key.get_pressed():
        m1.image=pygame.image.load('./animations/0.png')

    if pygame.key.get_pressed()[pygame.K_RIGHT]:
        for i in image_right:
            m2.image=pygame.image.load(i)
            image_right.remove(i)
            image_right.append(i)
            m2.rect.move_ip(1, 0)
    if pygame.key.get_pressed()[pygame.K_LEFT]:
        for i in image_left:
            m2.image=pygame.image.load(i)
            image_left.remove(i)
            image_left.append(i)
            m2.rect.move_ip(-1, 0)
    if True not in pygame.key.get_pressed():
        m2.image=pygame.image.load('./animations/0.png')

    
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            quit()
    pygame.display.flip()
    game_clock.tick(25)
