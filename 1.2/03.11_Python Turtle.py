import turtle

def draw(k=1):
    for i in range(360):
        t.pencolor(colors[i%6])
        t.width(i/100+1)
        t.forward(i*k)
        t.right(59)

        
screen=turtle.Screen()
screen.tracer(0)
t=turtle.Pen()
turtle.speed(10)
turtle.delay(0)
turtle.bgcolor('black')
t.hideturtle()
colors=['red','purple', 'blue', 'green', 'yellow', 'orange']
k=0.5
v=0.04
for i in range(10000):
    t.clear()
    draw(k)
    screen.update()
    t.home()
    t.right(i)
    k+=v
    if (k>5 or k<0.1):
        v=-v
