import tkinter as tk

def add():
        num1 = int(entry1.get()) #считывает преобразует введенное число в целочисленный тип
        num2 = int(entry2.get())
        result = num1 + num2
        result_label.config(text=str(result))

root = tk.Tk()
root.title("Программа сложения двух чисел") #заголовок

label1 = tk.Label(root, text="Введите первое число:")#подписи
label1.pack()

entry1 = tk.Entry(root)#поле для ввода значений
entry1.pack()

label2 = tk.Label(root, text="Введите второе число:")
label2.pack()

entry2 = tk.Entry(root)
entry2.pack()

add_button = tk.Button(root, text="Сложить", command=add)
add_button.pack()

result_label = tk.Label(root, text="Результат:")
result_label.pack()

result = tk.Label(root, text="")
result.pack()

root.mainloop()
