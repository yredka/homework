import pygame
import random

pygame.init()

pygame.mixer.music.load("snake.mp3")
pygame.mixer.music.play(-1, 0, 2)
pygame.mixer.music.set_volume(0.8)
eat = pygame.mixer.Sound('eat.mp3')
eat.set_volume(0.7)
hit = pygame.mixer.Sound('hit.mp3')
over = pygame.mixer.Sound('game_over.mp3')
over.set_volume(0.3)

yellow = (179, 179, 15)
red = (213, 50, 80)
green = (40, 114, 51)
white = (255, 255, 255)

width = 500
height = 500

dis = pygame.display.set_mode((width, height))
pygame.display.set_caption('Змейка')
clock = pygame.time.Clock()
snake_block = 10
snake_speed = 15
font_style = pygame.font.SysFont("bahnschrift", 15)
score_font = pygame.font.SysFont("comicsansms", 25)


def text_objects(text, font):
    text_surface = font.render(text, True, white)
    return text_surface, text_surface.get_rect()


def button(msg, x, y, w, h, ic, ac, action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(dis, ac, (x, y, w, h))

        if click[0] == 1 and action != None:
            action()
    else:
        pygame.draw.rect(dis, ic, (x, y, w, h))

    smallText = pygame.font.SysFont("comicsansms", 20)
    textSurf, textRect = text_objects(msg, smallText)
    textRect.center = ((x + (w / 2)), (y + (h / 2)))
    dis.blit(textSurf, textRect)


def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        dis.fill(green)

        message("Чтобы прочитать правила, удерживайте кнопку ПРАВИЛА ", white, 50, height/6)
        button("ИГРАТЬ", 100, 350, 100, 50, green, yellow, gameLoop)
        button("ПРАВИЛА", 300, 350, 100, 50, green, yellow, rules)

        pygame.display.update()
        clock.tick(15)


def rules():
    dis.fill(green)
    message("Правила игры: управляйте змейкой, съедая красные квадраты.", white, 20, 30)
    message("Если змейка сталкивается с собственным телом или краем экрана,", white, 20, 60)
    message("игра заканчивается.", white, 20, 90)


def your_score(score):
    value = score_font.render("Ваш счёт: " + str(score), True, yellow)
    dis.blit(value, [0, 0])


def snake(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(dis, yellow, [x[0], x[1], snake_block, snake_block])


def message(msg, color, mesg_width, mesg_height):
    mesg = font_style.render(msg, True, color)
    dis.blit(mesg, [mesg_width, mesg_height])


def gameLoop():
    game_over = False
    game_close = False
    x1 = width / 2
    y1 = height / 2
    x1_change = 0
    y1_change = 0
    snake_List = []
    Length_of_snake = 1
    foodx = round(random.randrange(0, width - snake_block) / 10) * 10
    foody = round(random.randrange(0, height - snake_block) / 10) * 10
    while not game_over:
        while game_close == True:
            dis.fill(green)  # заливка фона
            message("Вы проиграли! ", white, width/3, height/6)
            message("  Нажмите ESC для выхода или SPACE для повторной игры ", white, 50, height/5)
            your_score(Length_of_snake - 1)
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        game_over = True
                        game_close = False
                    if event.key == pygame.K_SPACE:
                        gameLoop()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x1_change = -snake_block
                    y1_change = 0
                elif event.key == pygame.K_RIGHT:
                    x1_change = snake_block
                    y1_change = 0
                elif event.key == pygame.K_UP:
                    y1_change = -snake_block
                    x1_change = 0
                elif event.key == pygame.K_DOWN:
                    y1_change = snake_block
                    x1_change = 0
        if x1 >= width or x1 < 0 or y1 >= height or y1 < 0:
            hit.play()
            over.play()
            game_close = True
        x1 += x1_change
        y1 += y1_change
        dis.fill(green)  # заливка
        pygame.draw.rect(dis, red, [foodx, foody, snake_block, snake_block])
        snake_Head = []
        snake_Head.append(x1)
        snake_Head.append(y1)
        snake_List.append(snake_Head)
        if len(snake_List) > Length_of_snake:
            del snake_List[0]
        for x in snake_List[:-1]:
            if x == snake_Head:
                game_close = True
        snake(snake_block, snake_List)
        your_score(Length_of_snake - 1)
        pygame.display.update()
        if x1 == foodx and y1 == foody:
            eat.play()
            foodx = round(random.randrange(0, width - snake_block) / 10) * 10
            foody = round(random.randrange(0, height - snake_block) / 10) * 10
            Length_of_snake += 1
        clock.tick(snake_speed)
    pygame.quit()
    quit()


game_intro()
gameLoop()
