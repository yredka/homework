import pygame
import math

pygame.init()

WIDTH, HEIGHT = 800, 600
WHITE = (255, 255, 255)
SUN_RADIUS = 30
FPS = 45

class Planet:
    def __init__(self, name, radius, distance, color, angular_speed):
        self.name = name
        self.radius = radius
        self.distance = distance
        self.color = color
        self.angular_speed = angular_speed
        self.angle = 0

    def update(self):
        self.angle += self.angular_speed

    def get_position(self):
        x = WIDTH // 2 + self.distance * math.cos(math.radians(self.angle))
        y = HEIGHT // 2 + self.distance * math.sin(math.radians(self.angle))
        return x, y

    def get_earth_days(self):
        # кол-во земных дней в зависимости от угла планеты относительно Солнца
        earth_days = (self.angle % 360) / 360 * 365
        return int(earth_days)

    def draw(self, screen):
        x, y = self.get_position()
        pygame.draw.circle(screen, self.color, (x, y), self.radius)

class SolarSystem:
    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption("Солнечная система")
        self.planets = [
            Planet("Солнце", SUN_RADIUS, 0, (255, 255, 0), 0),
            Planet("Меркурий", 2, 57.9, (169, 169, 169), 0.25),
            Planet("Венера", 6, 108.2, (255, 215, 0), 0.1),
            Planet("Земля", 10, 149.6, (0, 0, 255), 0.05),
            Planet("Марс", 7, 227.9, (255, 0, 0), 0.025),
            Planet("Юпитер", 23, 778.3, (255, 165, 0), 0.01),
            Planet("Сатурн", 33, 1427.0, (255, 215, 0), 0.005),
            Planet("Уран", 40, 2871.0, (0, 191, 255), 0.002),
            Planet("Нептун", 60, 4497.1, (0, 0, 139), 0.001)
        ]
        self.orbit_radii = [planet.distance for planet in self.planets]
        self.real_sizes = {
            "Солнце": "1 392 684 км",
            "Меркурий": "2 439.7 км",
            "Венера": "6 051.8 км",
            "Земля": "6 371 км",
            "Марс": "3 389.5 км",
            "Юпитер": "139 822 км",
            "Сатурн": "116 464 км",
            "Уран": "50 724 км",
            "Нептун": "49 244 км"
        }
        self.orbit_radii = [planet.distance for planet in self.planets]
        self.clock = pygame.time.Clock()
        self.scale = 1.0  # Масштаб



    def run(self):
        running = True
        info_font = pygame.font.Font(None, 24)
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.scale *= 1.01  # Увеличить масштаб на 10%
                    elif event.key == pygame.K_DOWN:
                        self.scale /= 1.01

            self.screen.fill((0, 0, 0))

            for planet in self.planets:
                planet.radius *= self.scale
                planet.distance *= self.scale
            self.orbit_radii = [planet.distance for planet in self.planets]

            for planet in self.planets:
                planet.update()
                planet.draw(self.screen)



            for radius in self.orbit_radii:
                pygame.draw.circle(self.screen, WHITE, (WIDTH // 2, HEIGHT // 2), int(radius), 1)



            info_text = None
            for planet in self.planets:
                if self.is_mouse_over(planet):
                    x, y = planet.get_position()
                    angle = planet.angle
                    real_size = self.real_sizes.get(planet.name, "Неизвестен")
                    info = f"{planet.name}\nРазмер: {real_size} \nРасстояние: {planet.distance} млн км\nУгол: {angle} градусов"
                    info_lines = info.split('\n')
                    info_surface = pygame.Surface((200, len(info_lines) * 20))
                    for i, line in enumerate(info_lines):
                        text = info_font.render(line, True, WHITE)
                        info_surface.blit(text, (0, i * 20))
                    self.screen.blit(info_surface, (10, 10))
                    break

            nya_text = info_font.render("Для масштабирования нажмите кнопку вверх/вниз", True, WHITE)
            self.screen.blit(nya_text, (10, HEIGHT - 30))

            earth_days = self.planets[3].get_earth_days()
            text = info_font.render(f"Земных дней: {earth_days}", True, WHITE)
            self.screen.blit(text, (WIDTH - 200, HEIGHT - 30))

            pygame.display.update()
            self.clock.tick(FPS)

        pygame.quit()

    def is_mouse_over(self, planet):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        x, y = planet.get_position()
        distance = math.sqrt((mouse_x - x) ** 2 + (mouse_y - y) ** 2)
        return distance <= planet.radius

if __name__ == "__main__":
    solar_system = SolarSystem()
    solar_system.run()
