import math
def task_1():
    a, b, c = 1,2,3
    a, b, c=c,a,b
    print(f"После обмена значениями: a = {a}, b = {b}, c = {c}")

def task_2_1():
    while True:
        try:
            a = int(input("Введите первое слагаемое: "))
            b = int(input("Введите второе слагаемое: "))
            print(f"Сумма = {a + b}")
            break
        except ValueError:
            print("Ошибка! Введите число")

def task_2_2():
    while True:
        try:
            n = int(input("Введите колличество чисел которое хотите сложить: "))
            break
        except ValueError:
            print("Ошибка! Введите целое число")
    summ = 0

    for i in range(n):
        while True:
            try:
                a = int(input(f"Введите слагаемое: "))
                summ += a
                break
            except ValueError:
                print("Ошибка! Введите число")

    print(f"Сумма = {summ}")

def task_3_1():
    while True:
        try:
            x = int(input("Введите число x от 0 до 100, которое хотите возвести в степень 5: "))
            if 0 <= x <= 100:
                res = x ** 5
                print(f"{x} в степени 5 равно {res}")
            else:
                print("x должно быть в диапазоне от 0 до 100")
            break
        except ValueError:
            print("Ошибка! Введите число")

def task_3_2():
    while True:
        try:
            x = int(input("Введите число x от 0 до 100, которое хотите возвести в степень 5: "))
            res=1
            if 0 <= x <= 100:
                for i in range (5):
                    res = res*x
                print(f"{x} в степени 5 равно {res}")
            else:
                print("Число x должно быть в диапазоне от 0 до 100.")
            break

        except ValueError:
            print("Ошибка! Введите число")

def task_4():
    def fib(number):
        a, b = 0, 1
        while a < number:
            a, b = b, a + b
        return a == number

    while True:
        try:
            n = int(input("Введите число от 0 до 250, чтобы проверить принадлежит ли оно числам Фибоначчи: "))
            if 0 <= n <= 250:
                if fib(n):
                    print(f"{n} принадлежит числам Фибоначчи")
                else:
                    print(f"{n} не принадлежит числам Фибоначчи")
            else:
                print("Число должно быть в диапазоне от 0 до 250")
            break

        except ValueError:
            print("Ошибка! Введите целое число")


def task_5():
    while True:
        try:
            month = int(input("Введите номер месяца (от 1 до 12), чтобы узнать время года: "))

            if 1 <= month <= 12:
                if 3 <= month <= 5:
                    print("Весна")
                elif 6 <= month <= 8:
                    print("Лето")
                elif 9 <= month <= 11:
                    print("Осень")
                else:
                    print("Зима")
            else:
                print("Некорректный номер месяца")
            break

        except ValueError:
            print("Ошибка! Введите целое число")

def task_6():
    while True:
        try:
            n = int(input("Введите число N: "))
            even_count = 0
            odd_count = 0
            sum_of_numbers = 0

            for num in range(1, n + 1):
                sum_of_numbers += num
                if num % 2 == 0:
                    even_count += 1
                else:
                    odd_count += 1

            print(f"Сумма чисел от 1 до {n}: {sum_of_numbers}")
            print(f"Количество четных чисел: {even_count}")
            print(f"Количество нечетных чисел: {odd_count}")
            break

        except ValueError:
            print("Ошибка! Введите целое число")

def task_7():
    import time

    while True:
        try:
            N = int(input("Введите число N (меньше 250), чтобы узнать его делители: "))
            start_time = time.perf_counter()
            c = 0
            for i in range(1, N ):
                if N % i == 0:
                    c += 1
            print(f"{N}: {c} делителей")
            break

        except ValueError:
            print("Ошибка! Введите целое число")
    end_time = time.perf_counter()
    print(f"Время выполнения: {end_time - start_time:.7f} секунд")


def task_8():
    print("Чтобы найти пифогоровы тройки")
    while True:
        try:
            N = int(input("Введите начальное значение интервала: "))
            M = int(input("Введите конечное значение интервала: "))

            trip = []

            for a in range(N, M + 1):
                for b in range(a, M + 1):
                    c = math.sqrt(a ** 2 + b ** 2)
                    if c.is_integer() and c <= M:
                        trip.append((a, b, int(c)))

            print("Пифагоровы тройки:")
            for triplet in trip:
                print(triplet)
            break

        except ValueError:
            print("Ошибка! Введите целое число")

def task_9():
    while True:
        print("Числа, делящиеся на каждую из своих цифр:")
        try:

            N = int(input("Введите начальное значение интервала: "))
            M = int(input("Введите конечное значение интервала: "))

            def div(number):
                for _ in str(number):
                    a = int(_)
                    if a == 0 or number % a != 0:
                        return False
                return True

            divisible_numbers = []

            for num in range(N, M + 1):
                if div(num):
                    divisible_numbers.append(num)
            print(divisible_numbers)
            break

        except ValueError:
            print("Ошибка! Введите целое число")

def task_10():
    def is_perfect_number(number):
        divisors_sum = 0
        for i in range(1, number):
            if number % i == 0:
                divisors_sum += i
        return divisors_sum == number

    try:
        N = int(input("Введите количество совершенных чисел для поиска (N < 5): "))
        if 0 < N < 5:
            perfect_numbers = []
            num = 1

            while len(perfect_numbers) < N:
                if is_perfect_number(num):
                    perfect_numbers.append(num)
                num += 1

            print(f"Первые {N} совершенных чисел:")
            for number in perfect_numbers:
                print(number)
        else:
            print("Число N должно быть больше 0 и меньше 5.")
    except ValueError:
        print("Ошибка! Введите целое число для N.")

def task_11():
    import time

    my_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # 1. Используем индексацию
    start_time = time.perf_counter()
    last_element = my_array[-1]
    end_time = time.perf_counter()
    print(f"Последний элемент (способ 1): {last_element}")
    print(f"Время выполнения: {end_time - start_time:.7f} секунд")

    # 2. Используем функцию len() и индексацию
    start_time = time.perf_counter()
    last_element = my_array[len(my_array) - 1]
    end_time = time.perf_counter()
    print(f"Последний элемент (способ 2): {last_element}")
    print(f"Время выполнения: {end_time - start_time:.7f} секунд")

    # 3. Используем метод pop()
    start_time = time.perf_counter()
    last_element = my_array.pop()
    end_time = time.perf_counter()
    print(f"Последний элемент (способ 3): {last_element}")
    print(f"Время выполнения: {end_time - start_time:.7f} секунд")

def task_12():
    my_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print("Массив в обратном порядке:",my_array[::-1])

def task_13():
    def recursive_sum(arr, n):
        if n <= 0:
            return 0
        else:
            return arr[n - 1] + recursive_sum(arr, n - 1)

    my_array = [1, 2, 3, 4, 5]

    total_sum = recursive_sum(my_array, len(my_array))

    print("Сумма элементов массива:", total_sum)

def task_14_1():
    import tkinter as tk

    def convert():
        try:
            rubles = float(rubles_entry.get())
            dollars = rubles / 96.04 #текущий курс доллара
            result_label.config(text=f"{rubles:.2f} рублей = {dollars:.2f} долларов")
        except ValueError:
            result_label.config(text="Ошибка! Введите число")

    root = tk.Tk()
    root.title("Конвертер рублей в доллары")

    rubles_label = tk.Label(root, text="Сумма в рублях:")
    rubles_label.pack()

    rubles_entry = tk.Entry(root)
    rubles_entry.pack()

    convert_button = tk.Button(root, text="Конвертировать", command=convert)
    convert_button.pack()

    result_label = tk.Label(root, text="")
    result_label.pack()

    root.mainloop()

def task_14_2():
    import tkinter as tk

    def convert():
        try:
            amount = float(amount_entry.get())
            if currency_var.get() == "rub_to_usd":
                result = amount / 96.04
                result_label.config(text=f"{amount:.2f} рублей = {result:.2f} долларов")
            elif currency_var.get() == "usd_to_rub":
                result = amount * 96.04
                result_label.config(text=f"{amount:.2f} долларов = {result:.2f} рублей")
        except ValueError:
            result_label.config(text="Ошибка! Введите число")

    root = tk.Tk()
    root.title("Конвертер валют")

    amount_label = tk.Label(root, text="Сумма:")
    amount_label.pack()

    amount_entry = tk.Entry(root)
    amount_entry.pack()

    currency_var = tk.StringVar(value="rub_to_usd")

    currency_frame = tk.Frame(root)
    currency_frame.pack()

    rub_to_usd_radio = tk.Radiobutton(currency_frame, text="Рубли в доллары", variable=currency_var, value="rub_to_usd")
    usd_to_rub_radio = tk.Radiobutton(currency_frame, text="Доллары в рубли", variable=currency_var, value="usd_to_rub")

    rub_to_usd_radio.pack(side="left")
    usd_to_rub_radio.pack(side="right")

    convert_button = tk.Button(root, text="Конвертировать", command=convert)
    convert_button.pack()

    result_label = tk.Label(root, text="")
    result_label.pack()

    root.mainloop()

def task_15():
    try:
        n = int(input("Введите количество строк для таблицы умножения(от 5 до 20): "))
        m = int(input("Введите количество столбцов для таблицы умножения(от 5 до 20): "))

        if 5 <= n <= 20 and 5 <= m <= 20:
            for i in range(1, n + 1):
                for j in range(1, m + 1):
                    print(f"{i} x {j} = {i * j}", end="\t")
                print()
        else:
            print("Количество строк и столбцов должно быть от 5 до 20")
    except ValueError:
        print("Ошибка! Введите целое число")

def task_16():
    field = [['_' for _ in range(10)] for _ in range(10)]

    coordinates = [(1, 1), (2, 1), (3, 1), (5, 5), (5, 6), (5, 7),
                   (8, 3), (9, 3), (9, 5), (9, 7), (7, 8), (0, 6),
                   (0, 7), (0, 8), (0, 9), (2, 7), (2, 8), (5, 1),
                   (6, 1), (2, 4),]

    for x, y in coordinates:
        field[x][y] = 'X'

    def print_field(field):
        print("  0 1 2 3 4 5 6 7 8 9")
        for i, row in enumerate(field):
            print(i, end=' ')
            for cell in row:
                print(cell, end=' ')
            print()

    print_field(field)


tasks = {
    "1": task_1,
    "2.1": task_2_1,
    "2.2": task_2_2,
    "3.1": task_3_1,
    "3.2": task_3_2,
    "4": task_4,
    "5": task_5,
    "6": task_6,
    "7": task_7,
    "8": task_8,
    "9": task_9,
    "10": task_10,
    "11": task_11,
    "12": task_12,
    "13": task_13,
    "14.1": task_14_1,
    "14.2": task_14_2,
    "15": task_15,
    "16": task_16,
}
while True:

    choice = input("Введите номер задачи или Q для выхода: ")

    if choice.lower() == "q":
        print("Выход")
        break

    task = tasks.get(choice)
    if task:
        task()
    else:
        print("Введите номер задачи от 1 до 16")
