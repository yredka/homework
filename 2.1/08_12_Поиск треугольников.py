import itertools

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Triangle:
    def __init__(self, p1, p2, p3):
        self.points = [p1, p2, p3]

    def S(self):
        return abs((self.points[0].x * (self.points[1].y - self.points[2].y) +
                    self.points[1].x * (self.points[2].y - self.points[0].y) +
                    self.points[2].x * (self.points[0].y - self.points[1].y)) / 2)

    def __eq__(self, other):
        return self.S() == other.S()

    def __ne__(self, other):
        return self.S() != other.S()

    def __lt__(self, other):
        return self.S() < other.S()

    def __gt__(self, other):
        return self.S() > other.S()

    def __le__(self, other):
        return self.S() <= other.S()

    def __ge__(self, other):
        return self.S() >= other.S()

points = []
with open('plist.txt', 'r') as file:
    data = file.read()

    coordinates = data.split('][')
    for coord in coordinates:
        coord = coord.replace('[', '').replace(']', '')
        x, y = map(int, coord.split(','))
        points.append(Point(x, y))

triangles = []

for triplet in itertools.combinations(points, 3):
    triangle = Triangle(*triplet)

    if triangle.S() != 0:
        triangles.append(triangle)


min_triangle = min(triangles, key=lambda x: x.S())
print("Минимальная площадь:", min_triangle.S())

max_triangle = max(triangles, key=lambda x: x.S())
print("Максимальная площадь:", max_triangle.S())
