import time
import threading
import sys


class Zverushka:
    def __init__(self):
        self.full = 10
        self.happiness = 10
        self.lock = threading.Lock()

    def feed(self):
        with self.lock:
            self.full += 1

    def play(self):
        if self.full > 0:
            with self.lock:
                self.full -= 1
                self.happiness += 1
        else:
            print("Зверушка слишком голодна для игры!")

    def status(self):
        return f"Сытость: {self.full}, Радость: {self.happiness}"

def reduce_values(pet):
    while pet.full > 0 and pet.happiness > 0:
        time.sleep(3)
        with pet.lock:
            pet.full -= 1
            pet.happiness -= 1
            print_pet(pet)
            print("1-накормить 2-поиграть 3-выйти")

        if pet.full <= 0:
            print("Зверушка голодна и грустна. Игра закончена")
            sys.exit()
        elif pet.happiness <= 0:
            print("Зверушка грустит и не хочет играть. Игра закончена")
            sys.exit()

def print_pet(pet):
    pet_ascii = [
        "\t",
        "  /\\_/\\",
        f" ( o.o )",
        "\t",
        f"Сытость: {'#' * pet.full}{'-' * (10 - pet.full)}",
        f"Радость: {'#' * pet.happiness}{'-' * (10 - pet.happiness)}"
    ]

    for line in pet_ascii:
        print(line)

def main():
    pet = Zverushka()
    reduce_thread = threading.Thread(target=reduce_values, args=(pet,))
    reduce_thread.daemon = True
    reduce_thread.start()

    print("Привет! Добро пожаловать к вашей зверушке!")

    try:
        while pet.full > 0 and pet.happiness > 0:
            print_pet(pet)
            print("1-накормить 2-поиграть 3-выйти")
            choice = input()

            if choice == "1":
                pet.feed()
                print("Вы покормили зверушку!")
            elif choice == "2":
                pet.play()
                print("Вы поиграли с зверушкой!")
            elif choice == "3":
                print("Вы вышли из игры")
                break
            else:
                print("Пожалуйста, выберите действие 1, 2 или 3")

        if pet.full <= 0:
            print("Зверушка голодна и грустна. Игра закончена")
        elif pet.happiness <= 0:
            print("Зверушка грустит и не хочет играть. Игра закончена")

    except KeyboardInterrupt:
        print("Игра завершена")

if __name__ == "__main__":
    main()
