class My_list(list):

    def __sub__(self, other):
        if not isinstance(other, My_list):
            other = My_list(other)
        result = []
        for i in self:
            if i not in other:
                result.append(i)
        return result

    def __truediv__(self, div):
        if div == 0:
            raise ValueError("На ноль делить нельзя")
        result = [i / div for i in self]
        return My_list(result)

    def display(self):
        super().__str__()


l1 = My_list([1, 2, 3, 4, 5])
l2 = My_list([3, 4, 5, 6, 7])

result_sub = l1 - l2
print("Вычитание:", result_sub)

result_div = l1 / 2
print("Деление:", result_div)

print("l1:", l1)
print("l2:", l2)

l1.display()
